<?php
class Actor
{
  protected $id,
            $nom,
            $prenom;
  

  public function __construct(array $donnees)
  {
    $this->hydrate($donnees);
     
  }
  
  
  public function hydrate(array $donnees)
  {
    foreach ($donnees as $key => $value)
    {
      $method = 'set'.ucfirst($key);
      
      if (method_exists($this, $method))
      {
        $this->$method($value);
      }
    }
  }
  
  
  public function nom()
  {
    return $this->nom;
  }
  
  public function prenom()
  {
    return $this->prenom;
  }
  
  
  public function id()
  {
    return $this->id;
  }
  
  
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->id = $id;
    }
  }
  
  public function setNom($nom)
  {
      $this->nom = $nom;
  }
  
  public function setPrenom()
  {
    $this->prenom = $prenom;
  }
  
}