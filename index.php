```php

<?php
include __DIR__ . './Model/FilmRepository.php';
include __DIR__ . './Model/ActorRepository.php';
include __DIR__ . './Model/CastingRepository.php';

switch ($_SERVER['REQUEST_URI']) {
    case '/help':
        include 'help.php';
        break;
    case '/calendar':
        include 'calendar.php';
        break;
    default:
        include 'notfound.php';
        break;
}
?>
